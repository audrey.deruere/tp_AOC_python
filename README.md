# Prerequisites
- [Python 2.7](https://www.python.org/downloads/release/python-353/)

# Quickstart
- clone repository in your computer and then with a terminal, launch the file main.py or main2.py
- main.py allow you to go from a point A to a point B with search of shortest path
- main2.py allow you to go from a point A to a multiple point randomly define with search of shortest path but it's not finish.
- The programm run and then display result

