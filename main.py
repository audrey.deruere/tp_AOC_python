import random
import math
import matplotlib.pyplot as plt
import numpy as np
import xlrd
import networkx as nx

idTenant=0
idAboutissant=0
lon=0
u=0
v=0

"""Reading File"""
# book = xlrd.open_workbook("./VOIES_NM_csv/VOIES_NM.xlsx")
book = xlrd.open_workbook("./VOIES_NM_COMMUNE_csv/VOIES_NM_440009.xlsx")


sh = book.sheet_by_index(0)
ids = sh.col_values(6, start_rowx=1)
libelle = sh.col_values(5, start_rowx=1)
idTenant = sh.col_values(8, start_rowx=1)
idAboutissant = sh.col_values(10, start_rowx=1)
lon = sh.col_values(13, start_rowx=1)

myStreet = zip(ids,libelle, idTenant, idAboutissant, lon)

"""Graph creation"""
"""Complete graph"""
G=nx.Graph()
for i,lib, idT,idA,l in myStreet:
		if(idT!=0 and idA !=0):
			G.add_edge(int(i), int(idT), rue = lib, weigth=abs(l/2), pheromone = 0)
			G.add_edge(int(i), int(idA), rue = lib, weigth=abs(l/2), pheromone = 0)
		if(idT!=0 and idA ==0):
			G.add_edge(int(i),int(idT), rue = lib, weigth=abs(l), pheromone = 0)
		if(idT==0 and idA !=0):
			G.add_edge(int(i), int(idA), rue = lib, weigth=abs(l), pheromone = 0)
for i,lib, idT,idA,l in myStreet:
	if(idT==0 and idA ==0):
		for i,lib, idT,idA,l in myStreet:
			if(idT==i):
				G.add_edge(int(i), int(idT), rue = lib, weigth=abs(l), pheromone = 0)
			if(idA==i):
				G.add_edge(int(i), int(idA), rue = lib, weigth=abs(l), pheromone = 0)

""" Source and target are used for verification of best path when ant should go from A to B"""
source = 1
target = 30
shortPath = nx.shortest_path(G,source=source,target=target)
print "\n------------------------------------------------------------"
print "                     Results                                "
print "------------------------------------------------------------"
print "Chemin le plus court (valeurs generees par networkx pour comparer) : " + str(shortPath)
pos=nx.spring_layout(G)
edge_labels=dict([((u,v,),d['rue'])
             for u,v,d in G.edges(data=True)])
nx.draw_networkx_edge_labels(G,pos,edge_labels=edge_labels)
nx.draw(G,pos)
plt.show()

"""ACO"""
"""constante"""
nbAnt = 2
nbcycle=50
nb_street= len(myStreet)
c=1
nodeInitial=1
nodeFinal=30
e = nodeInitial
find_path=[]
path_length=[]
path=[]
badPath= []
selection = []
y=[]
f = []
ant = {'initial_pheromone':1,'alpha':1,'beta':2,'pheromone_deposit':1,'evaporation_constant':0.8}



# def defineRandomCustomer(nb_street):
# 	for r in range(10):
# 		customer['node'].append(random.randrange(0, nb_street,1))
# 	return customer


""" function allow ant to choose next best node with a delation of the bad path and a visibilty of pheromone on next edge"""
def getNextNode(e,path,nodeInitial,nodeFinal,badPath,ant):
	neighbors = G.neighbors(e)
	print 'voisin avant delete : ' + str(neighbors)
	newNeighbors = deleteBadPath(e,neighbors,path,badPath,nodeFinal)
	if(len(newNeighbors)==0):
		nextNode = returnStateBefore(e,path,badPath)
	else:
		havePheromone = havePheromoneEdge(e,newNeighbors)
		
		nextNode = chooseBestNextNode(e,newNeighbors,havePheromone,ant)
	return nextNode

""" function calculation of inverse of the distance"""
def calcVisibility(e, p):
	w=getWeightEdge(e,p)
	attraction = 1/w
	return attraction

""" function calculation numerator of intensity calcul"""
def calcIntensity(e,p, ant):
        calc = (math.pow(getPheromoneEdge(e,p), ant['alpha']))*(math.pow(calcVisibility(e,p),ant['beta']))
        return calc

"""function calculation denominator of intensity calcul"""
def calcIntensity2(newNeighbors,e,ant):
	intensity = 0
	for k in newNeighbors:
		intensity = intensity + calcIntensity(e,k, ant)
	return intensity

"""Allow ant to know if the next edge have pheromone"""
def havePheromoneEdge(e,newNeighbors):
	b =  False
	for t in newNeighbors:
		if(getPheromoneEdge(e,t)>0.0):
			b=True
	print 'Est ce quil y a des pheromone : ' + str(b)
	return b

""" With neighbors tab allow ant to choose the best next node"""
def chooseBestNextNode(e,newNeighbors,havePheromone,ant):
	weigth=[]
	choice = []
	neighborsL=len(newNeighbors)
	if(neighborsL>1):
		if(havePheromone==True):
			for r in newNeighbors:
				firstcalc = calcIntensity(e,r, ant)
				print 'first calc : ' + str(firstcalc)
				secondcalc = calcIntensity2(newNeighbors,e,ant)
				print 'second calc : ' + str(secondcalc)
				
				choice.append(firstcalc/secondcalc)
				print 'choice : ' + str(choice)
			randome = random.random()
			if(randome >0.6):
				print 'RANDOM PHEROMONE: ' + str(randome)
				n = newNeighbors[choice.index(max(choice))]
			else:
				print 'RANDOM RANDOM: ' + str(randome)
				n = newNeighbors[random.randrange(0, neighborsL-1, 1)]
		else:
			n = newNeighbors[random.randrange(0, neighborsL-1, 1)]

	else:
		n = newNeighbors[0]

	print 'Le prochain node choisi : ' + str(n)
	return n

""" function to delete of neighbors tab the bad path discover before"""
def deleteBadPath(e,neighbors,path,badPath,nodeFinal):
	print 'mon actuelle tableau de mauvais chemin : ' + str(badPath)
	listToDelete = []
	for a in neighbors:
		futurNeighbors = G.neighbors(a)
		print 'Les futurs voisin possible : ' + str(futurNeighbors) + ' pour le noeud futur : ' + str(a) + ' et pour le noeud actuel : ' + str(e)
		# if((len(futurNeighbors)==1 and e in futurNeighbors and a!=nodeFinal) or a in badPath or a in path or (len(futurNeighbors)==2 and e in futurNeighbors and a in futurNeighbors)):
		if(a in badPath or a in path):
			# if(len(futurNeighbors)==2 and e in futurNeighbors and a in futurNeighbors):
			print 'remove and take in bath path'
			# if a not in badPath:
			# 	badPath.append(a)
			print 'just remove it : ' + str(a)
			listToDelete.append(a)
		# else:
		# 	print 'remove and take in bad path' + str(a)
		# 	if a not in badPath:
		# 		badPath.append(a)
		# 	listToDelete.append(a)
		else:
			print 'Im good : ' + str(a)

	for y in listToDelete:
		neighbors.remove(y)
	print 'voisin apres delete : ' + str(neighbors)
	return neighbors

""" Construction of the path during the discovery"""
def definePath(i,path):
	path.append(i)
	if(len(path)>1 and path[-1]==path[-2]):
		path.remove(path[-1])
	return path

""" Allow ant to return in a before state if she can't do another mouvement"""
def returnStateBefore(e,path,badPath):
	print 'retour dans le temps -1'
	if path[-1] not in badPath:
		badPath.append(path[-1])
	path.remove(path[-1])
	f=path[-1]
	print 'on repart de : ' + str(f)
	return f

""" Update pheromone on edge between the actual node and node discover before""" 
def updatePheromone(path,ant):
	for i, j in zip(path, path[1:]):
		pheromoneEdge = G[i][j]['pheromone']
		weigth = G[i][j]['weigth']
		print 'ma route sapelle : ' + str((G[i][j]['rue']).encode('utf-8')) + 'i : ' + str(i) + 'j : '+ str(j)
		pheromone = pheromoneEdge + (ant['pheromone_deposit']/weigth)
		print 'mes pheromone ont augmente et sont maintenant de :' + str(pheromone)
		G[i][j]['pheromone']=pheromone

""" function to evaporate pheromone at each turn"""
def evaporate(path,ant):
	for i, j in zip(path, path[1:]):
		pheromoneEdge = G[i][j]['pheromone']
		print 'ma route sapelle : ' + str((G[i][j]['rue']).encode('utf-8')) + 'i : ' + str(i) + 'j : '+ str(j)
		pheromone = pheromoneEdge*(1-ant['evaporation_constant'])
		print 'Les pheromones ont diminue et sont maintenant de : ' + str(pheromone)
		G[i][j]['pheromone']=pheromone

""" Function to retrieve the number of pheromone of an edge"""
def getPheromoneEdge(i,nextNode):
	pheromoneEdge = G[i][nextNode]['pheromone']
	print ('PHEROMONE DE MON ARC : ' + str(pheromoneEdge))
	return pheromoneEdge

""" Function to retrieve the weight of an edge"""
def getWeightEdge(i,nextNode):
	weight = G[i][nextNode]['weigth']
	print ('POID DE MON ARC : ' + str(weight))
	return weight

""" Function to retrieve the name of an edge"""
def getNameEdge(i,nextNode):
	name = G[i][nextNode]['rue'].encode('utf-8')
	return name

""" Function to calculate path length when a solution is discover"""
def calcPathLength(path):
	w = 0
	for i, j in zip(path, path[1:]):
		weightEdge = G[i][j]['weigth']
		w = w + weightEdge
	print 'LE POID DE MON CHEMIN : ' + str(w)
	return w

""" After all cycles are finished, selection of the best path in a solution tab"""
def selectBestPath(path):
	print 'PATH EST LENSEMBLE DE SOLUTION : ' + str(path)
	path_length=[]
	for f in path:
		print 'F EST UNE SOLUTION : ' + str(f)
		path_length.append(calcPathLength(f))
		print 'ENSEMBLE DES POIDS DUNE SOLUTION : ' + str(path_length)
		print 'INDEX DE LA SOLUTION POIDS CHOISIE : ' + str(path_length.index(min(path_length)))
	print 'INDEX DE LA SOLUTION CHOISIE : ' + str(path[path_length.index(min(path_length))])
	y.append(path_length[path_length.index(min(path_length))])
	return path[path_length.index(min(path_length))]




""" Launch of algorithm"""

f.append(c)
# customer = defineRandomCustomer(nb_street)
while c < nbcycle:
	j=0
	# badPath=[]
	print "\n------------------------------------------------------------"
	print "                     MY CYCLE                                "
	print "------------------------------------------------------------"
	print "CYCLE : " + str(c)
	while j < nbAnt:
		print "\n------------------------------------------------------------"
		print "                     MY ANT                                  "
		print "------------------------------------------------------------"
		print "ITERATION : " + str(j)
		path=[]
		find_path = []
		e=nodeInitial
		while (e != nodeFinal):
			path = definePath(e,path)
			print 'ma solution actuelle : ' + str(path)
			nextNode = getNextNode(e,path,nodeInitial,nodeFinal,badPath,ant)
			if(nextNode==nodeFinal):
				path = definePath(nextNode,path)
				print 'solution : ' + str(path)
				e=nextNode
			else:
				e=nextNode		
		find_path.append(path)
		print 'PATH TROUVE : ' + str(find_path)
		updatePheromone(path,ant)
		evaporate(path,ant)
		j = j +1
	selection.append(selectBestPath(find_path))
	print 'MEILLEUR SELECTION POUR LE CYCLE : ' + str(selection)

	c = c + 1
	f.append(c)
MyshortestPath = selectBestPath(selection)
print "\n------------------------------------------------------------"
print "                     Results                                "
print "------------------------------------------------------------"
print 'RESULTAT CHEMIN : ' + str(MyshortestPath)
print 'RESULTAT LONGUEUR : ' + str(calcPathLength(MyshortestPath))

print "\n------------------------------------------------------------"
print "                     Le chemin                                "
print "------------------------------------------------------------"
for q, s in zip(MyshortestPath, MyshortestPath[1:]):
	print 'Rue '+ str(MyshortestPath.index(s))+' : ' + str(getNameEdge(q,s))


"""Path graph"""
""" Solution graph"""
G2=nx.Graph()
for i,lib, idT,idA,l in myStreet:
	if(i in MyshortestPath):
			if(idT!=0 and idA !=0):
				G2.add_edge(int(i), int(idT), rue = i, weigth=abs(l/2))
				G2.add_edge(int(i), int(idA), rue = i, weigth=abs(l/2))
			if(idT!=0 and idA ==0):
				G2.add_edge(int(i),int(idT), rue = i, weigth=abs(l))
			if(idT==0 and idA !=0):
				G2.add_edge(int(i), int(idA), rue = i, weigth=abs(l))
for i,lib, idT,idA,l in myStreet:
	if(i in MyshortestPath):
		if(idT==0 and idA ==0):
			for i,lib, idT,idA,l in myStreet:
				if(idT==i):
					G.add_edge(int(i), int(idT), rue = lib, weigth=abs(l), pheromone = 0)
				if(idA==i):
					G.add_edge(int(i), int(idA), rue = lib, weigth=abs(l), pheromone = 0)

pos=nx.spring_layout(G2)
nx.draw_networkx_nodes(G2,pos,nodelist=MyshortestPath,node_color='b',node_size=500)
edge_labels=dict([((u,v,),d['rue'])
             for u,v,d in G2.edges(data=True)])
nx.draw_networkx_edge_labels(G2,pos,edge_labels=edge_labels)
nx.draw(G2,pos)
plt.show()

plt.subplot(211)
plt.title("Longueur du chemin en fonction du nombre d'iteration realisee")
print ' longueur de f : ' + str(y)

""" Graph of weight of path found for each iteration"""
plt.plot(f,y,"r--")
# plt.plot(x,y,"r--")
plt.ylabel('poid du chemin')
plt.xlabel("nombre d'iteration")
plt.show()
