import random
import math
import matplotlib.pyplot as plt
import xlrd
import networkx as nx
import collections
import numpy as np

idTenant=0
idAboutissant=0
lon=0
u=0
v=0

"""Reading File"""

#BIG FILE
#PLEASE CHANGE PATH IF YOU TEST IN ANOTHER COMPUTER
# book = xlrd.open_workbook("./VOIES_NM.xlsx")

#BASSE-GOULAINE FILE
#PLEASE CHANGE PATH IF YOU TEST IN ANOTHER COMPUTER
book = xlrd.open_workbook("./VOIES_NM_440009.xlsx")


sh = book.sheet_by_index(0)
ids = sh.col_values(6, start_rowx=1)
libelle = sh.col_values(5, start_rowx=1)
idTenant = sh.col_values(8, start_rowx=1)
idAboutissant = sh.col_values(10, start_rowx=1)
lon = sh.col_values(13, start_rowx=1)

myStreet = zip(ids,libelle, idTenant, idAboutissant, lon)

"""Graph creation"""
"""Complete graph"""

G=nx.Graph()
for i,lib, idT,idA,l in myStreet:
		if(idT!=0 and idA !=0):
			G.add_edge(int(i), int(idT), rue = lib, weigth=abs(l/2), pheromone = 0)
			G.add_edge(int(i), int(idA), rue = lib, weigth=abs(l/2), pheromone = 0)
		if(idT!=0 and idA ==0):
			G.add_edge(int(i),int(idT), rue = lib, weigth=abs(l), pheromone = 0)
		if(idT==0 and idA !=0):
			G.add_edge(int(i), int(idA), rue = lib, weigth=abs(l), pheromone = 0)
for i,lib, idT,idA,l in myStreet:
	if(idT==0 and idA ==0):
		for i,lib, idT,idA,l in myStreet:
			if(idT==i):
				G.add_edge(int(i), int(idT), rue = lib, weigth=abs(l), pheromone = 0)
			if(idA==i):
				G.add_edge(int(i), int(idA), rue = lib, weigth=abs(l), pheromone = 0)

source = 1
target = 30
shortPath = nx.shortest_path(G,source=source,target=target)
print "\n------------------------------------------------------------"
print "                     NETWORK Results                           "
print "------------------------------------------------------------"
print "Chemin le plus court (valeurs generees par networkx pour comparer) : " + str(shortPath)
pos=nx.spring_layout(G)
edge_labels=dict([((u,v,),d['rue'])
             for u,v,d in G.edges(data=True)])
nx.draw_networkx_edge_labels(G,pos,edge_labels=edge_labels)
nx.draw(G,pos)

# plt.show()

"""ACO"""
"""constante"""
nbAnt = 1
nbcycle=1
nb_street= len(myStreet)
j=0
c=0
nodeInitial=1
nodeFinal=30
find_path=[]
path_length=[]
path=[]
badPath=[]
selection = []

ant = {'initial_pheromone':1,'alpha':1,'beta':3,'pheromone_deposit':1,'evaporation_constant':0.8}


"""ACO"""
"""Fonction"""
def defineRandomCustomer(nb_street):
	Allcustomer = []
	#for testing with good path

	# intermediaire = [1,64,204,135,8,126]
	# for r in range(6):
	# 	customer=[]
	# 	customer.append(intermediaire[r])
	# 	customer.append(False)
	# 	Allcustomer.append(customer)

	#for testing in real condition

	for r in range(4):
		customer=[]
		firstCustomer = random.sample(G.nodes(), 1)
		customer.append(firstCustomer[0])
		while(customer[-1] in Allcustomer and customer[-1]):
			customer.remove(customer[-1])
			firstCustomer = random.sample(G.nodes(), 1)
			customer.append(firstCustomer[0])
		customer.append(False)
		print 'CUSTOMER ' + str(r) + ' : ' + str(customer)
		Allcustomer.append(customer)
		print 'ALL CUSTOMER  : ' + str(Allcustomer)
	return Allcustomer

def getNextNode(e,path,nodeInitial,Allcustomer,badPath,ant):
	neighbors = G.neighbors(e)
	print 'voisin avant delete : ' + str(neighbors)
	newNeighbors = deleteBadPath(e,neighbors,path,badPath,nodeFinal,Allcustomer)
	if(len(newNeighbors)==0):
		nextNode = returnStateBefore(e,path,badPath)
	else:
		havePheromone = havePheromoneEdge(e,newNeighbors)
		nextNode = chooseBestNextNode(e,newNeighbors,havePheromone,ant, Allcustomer)
	return nextNode

def calcVisibility(e, p):
	w=getWeightEdge(e,p)
	attraction = 1/w
	return attraction

def calcIntensity(e,p, ant):
        calc = (math.pow(getPheromoneEdge(e,p), ant['alpha']))*(math.pow(calcVisibility(e,p),ant['beta']))
        return calc

def calcIntensity2(newNeighbors,e,ant):
	intensity = 0
	for k in newNeighbors:
		intensity = intensity + calcIntensity(e,k, ant)
	return intensity

def havePheromoneEdge(e,newNeighbors):
	b =  False
	for t in newNeighbors:
		if(getPheromoneEdge(e,t)>0.0):
			b=True
	print 'Est ce quil y a des pheromone : ' + str(b)
	return b

def checkIfInPath(newNeighbors, Allcustomer):
	solutionNeighbors = []
	for n in newNeighbors:
		for a in Allcustomer:
			if(n in a and a[1]==False):
				solutionNeighbors.append(n)
	if(len(solutionNeighbors)!=0):
		print 'VOISINS BESOIN : ' +str(solutionNeighbors)
		return solutionNeighbors
	else:
		print 'MEME VOINSINS'
		return newNeighbors

def chooseBestNextNode(e,newNeighbors,havePheromone,ant, Allcustomer):
	weigth=[]
	choice = []
	intensity = 0
	neighborsL=len(newNeighbors)
	if(havePheromone==True):
		p = checkIfInPath(newNeighbors, Allcustomer)
		if(len(p)!=len(newNeighbors)):
			s = random.sample(p, 1)
			n=s[0]
		else:
			for r in newNeighbors:
				firstcalc = calcIntensity(e,r, ant)
				secondcalc = calcIntensity2(newNeighbors,e,ant)
				choice.append(firstcalc/sum(secondcalc))
			n = newNeighbors[choice.index(max(choice))]
	else:
		p = checkIfInPath(newNeighbors, Allcustomer)
		if(len(p)!=len(newNeighbors)):
			s = random.sample(p, 1)
			n=s[0]

		else:
			s = random.sample(newNeighbors, 1)
			n=s[0]

	print 'Le prochain node choisi : ' + str(n)
	return n

def deleteBadPath(e,neighbors,path,badPath,nodeFinal,customer):
	print 'mon actuelle tableau de mauvais chemin : ' + str(badPath)
	a=0
	listToDelete = []
	listCustomer = []
	for c in customer:
		listCustomer.append(c[0])
	print 'MA LISTE DE CUSTOMER POUR DELETE OU NON : ' + str(listCustomer)
	for a in neighbors:
		futurNeighbors = G.neighbors(a)
		print 'Les futurs voisin possible : ' + str(futurNeighbors) + ' pour le noeud futur : ' + str(a) + ' et pour le noeud actuel : ' + str(e)
		# if((len(futurNeighbors)==1 and a not in listCustomer) or a in badPath or (len(futurNeighbors)==2 and e in futurNeighbors and futurNeighbors not in listCustomer)or (e==a)):
		# 	if(a in badPath or a in path):
		# 		if(len(futurNeighbors)==2 and e in futurNeighbors and futurNeighbors not in futurNeighbors):
		# 			if(a not in badPath):
		# 				badPath.append(a)
		# 			listToDelete.append(a)
		# 		else:
		# 			listToDelete.append(a)
		# 	else:
		# 		if(a not in badPath):
		# 			badPath.append(a)
		# 		listToDelete.append(a)
		# if():
		# 	# if(len(futurNeighbors)==2 and e in futurNeighbors and a in futurNeighbors):
		# 	print 'remove and take in bath path'
		# 	# if a not in badPath:
		# 	# 	badPath.append(a)
		# 	print 'just remove it : ' + str(a)
		# 	listToDelete.append(a)
		if((a in badPath) or (len(futurNeighbors)==1 and e in futurNeighbors)):
				listToDelete.append(a)
		# else:
		# 	print 'remove and take in bad path' + str(a)
		# 	if a not in badPath:
		# 		badPath.append(a)
		# 	listToDelete.append(a)
		

	for y in listToDelete:
		neighbors.remove(y)
	print 'voisin apres delete : ' + str(neighbors)
	return neighbors

def definePath(i,path):
	if(len(path)==0):
		path.append(i)
	else: 
		if(i!=path[-1]):
			path.append(i)
		if(len(path)>1 and path[-1]==path[-2]):
			path.remove(path[-1])
	return path

def returnStateBefore(e,path,badPath):
	print 'RETOUR EN ARRIERE YATAAAAA'
	if path[-1] not in badPath:
		badPath.append(path[-1])
	path.pop()
	f=path[-1]
	print 'on reprend de : ' +str(f)
	return f

def updatePheromone(path,ant):
	for i, j in zip(path, path[1:]):
		print 'update PHEROMONE in edge : ' + str(i) + ' / ' + str(j)
		pheromoneEdge = G[i][j]['pheromone']
		weigth = G[i][j]['weigth']
		pheromone = pheromoneEdge + (ant['pheromone_deposit']/weigth)
		G[i][j]['pheromone']=pheromone

def evaporate(path,ant):
	for i, j in zip(path, path[1:]):
		pheromoneEdge = G[i][j]['pheromone']
		pheromone = pheromoneEdge*(1-ant['evaporation_constant'])
		G[i][j]['pheromone']=pheromone

def getPheromoneEdge(i,nextNode):
	pheromoneEdge = G[i][nextNode]['pheromone']
	return pheromoneEdge

def getWeightEdge(i,nextNode):
	weight = G[i][nextNode]['weigth']
	return weight

def getNameEdge(i,nextNode):
	name = G[i][nextNode]['rue'].encode('utf-8')
	return name

def calcPathLength(path):
	w = 0
	for i, j in zip(path, path[1:]):
		weightEdge = G[i][j]['weigth']
		w = w + weightEdge
	return w

def selectBestPath(path):
	print 'PATH EST LENSEMBLE DE SOLUTION : ' + str(path)
	path_length=[]
	for f in path:
		path_length.append(calcPathLength(f))
	return path[path_length.index(min(path_length))]

def PathCustomerVerification(path,customer):
	customerL = len(customer)
	nbCustomerDelivered = 0
	for c in customer:
		print 'C CUSTOMER verification : ' + str(c)
		if(c[1]==True):
			nbCustomerDelivered = nbCustomerDelivered +1
	if(customerL==nbCustomerDelivered):
		return True
	else:
		return False

def customerUpdate(nextNode,Allcustomer):
	for c in Allcustomer:
		if(nextNode in c):
			c[1]=True

def reloadCustomerInformation(Allcustomer):
	for a in Allcustomer:
		a[1]=False

#
# MY CYCLE OF ANTS - DISCOVER THE SHORT PATH
#

Allcustomer = defineRandomCustomer(nb_street)
while c < nbcycle:
	j=0
	z=0
	nextNode=0
	badPath=[]
	print "\n------------------------------------------------------------"
	print "                     MY CYCLE                                "
	print "------------------------------------------------------------"
	print "CYCLE : " + str(c)
	while (j < nbAnt):
		print "\n------------------------------------------------------------"
		print "                     MY ANT                                  "
		print "------------------------------------------------------------"
		print "ITERATION : " + str(j)
		path=[]
		find_path = []
		e=nodeInitial
		customerUpdate(e,Allcustomer)
		while (PathCustomerVerification(path,Allcustomer) != True):
			path = definePath(e,path)
			print 'ma solution actuelle : ' + str(path)
			nextNode = getNextNode(e,path,nodeInitial,Allcustomer,badPath,ant)
			customerUpdate(nextNode,Allcustomer)
			if(PathCustomerVerification(path,Allcustomer)==True):
				path = definePath(nextNode,path)
				print 'solution : ' + str(path)
			else:
				e=nextNode
		find_path.append(path)
		print 'PATH TROUVE : ' + str(find_path)
		updatePheromone(path,ant)
		evaporate(path,ant)
		j = j +1
	selection.append(selectBestPath(find_path))
	print 'MEILLEUR SELECTION POUR LE CYCLE : ' + str(selection)
	reloadCustomerInformation(Allcustomer)
	c = c + 1
MyshortestPath = selectBestPath(selection)
print "\n------------------------------------------------------------"
print "                     Results                                "
print "------------------------------------------------------------"
print 'RESULTAT CHEMIN : ' + str(MyshortestPath)
print 'RESULTAT LONGUEUR : ' + str(calcPathLength(MyshortestPath))

print "\n------------------------------------------------------------"
print "                     Le chemin                                "
print "------------------------------------------------------------"
for q, s in zip(MyshortestPath, MyshortestPath[1:]):
	print 'Rue '+ str(MyshortestPath.index(s))+' : ' + str(getNameEdge(q,s))


"""Short Path graph"""

G2=nx.Graph()
for i,lib, idT,idA,l in myStreet:
	if(i in MyshortestPath):
			if(idT!=0 and idA !=0):
				G2.add_edge(int(i), int(idT), rue = i, weigth=abs(l/2))
				G2.add_edge(int(i), int(idA), rue = i, weigth=abs(l/2))
			if(idT!=0 and idA ==0):
				G2.add_edge(int(i),int(idT), rue = i, weigth=abs(l))
			if(idT==0 and idA !=0):
				G2.add_edge(int(i), int(idA), rue = i, weigth=abs(l))

for i,lib, idT,idA,l in myStreet:
	if(idT==0 and idA ==0 and i in MyshortestPath):
		for i,lib, idT,idA,l in myStreet:
			if(idT==i):
				G.add_edge(int(i), int(idT), rue = lib, weigth=abs(l), pheromone = 0)
			if(idA==i):
				G.add_edge(int(i), int(idA), rue = lib, weigth=abs(l), pheromone = 0)

pos=nx.spring_layout(G2)
nx.draw_networkx_nodes(G2,pos,nodelist=MyshortestPath,node_color='b',node_size=500)
edge_labels=dict([((u,v,),d['rue'])
             for u,v,d in G2.edges(data=True)])
nx.draw_networkx_edge_labels(G2,pos,edge_labels=edge_labels)
nx.draw(G2,pos)
plt.show()

